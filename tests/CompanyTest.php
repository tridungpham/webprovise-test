<?php

use PHPUnit\Framework\TestCase;

/**
 * @covers
 */
final class CompanyTest extends TestCase
{
    /**
     * @test
     */
    public function can_init_company_from_array_of_data()
    {
        $data = [
            'id' => 'company-1',
            'name' => 'ACME inc',
            'parentId' => 'parent-1',
            'createdAt' => '2020-08-27T00:22:26.927Z',
        ];

        $company = new Company($data);
        $this->assertSame($data['id'], $company->getId());
        $this->assertSame($data['name'], $company->getName());
        $this->assertSame($data['parentId'], $company->getParentId());
        $this->assertInstanceOf(DateTime::class, $company->getCreatedTime());
        $this->assertEquals('2020-08-27', $company->getCreatedTime()->format("Y-m-d"));
    }

    /**
     * @test
     */
    public function should_ignore_parent_id_that_is_not_a_string()
    {
        $data = [
            'id' => 'company-1',
            'name' => 'ACME inc',
            'parentId' => 0,
        ];

        $company = new Company($data);
        $this->assertSame($data['id'], $company->getId());
        $this->assertSame('0', $company->getParentId());
        $this->assertTrue($company->isFirstLevelCompany());
    }

    /**
     * @test
     */
    public function can_add_subsidiary()
    {
        $parentCompany = new Company(['id' => "1", 'name' => 'ACME GLOBAL', 'parentId' => '0']);
        $childCompany1 = new Company(["id" => "2", 'name' => 'ACME Asia', 'parentId' => '1']);
        $childCompany2 = new Company(["id" => "3", 'name' => 'ACME Europe', 'parentId' => '1']);

        $this->assertCount(0, $parentCompany->getSubsidiaries());
        $parentCompany->addSubsidiary($childCompany1);
        $parentCompany->addSubsidiary($childCompany2);

        $this->assertCount(2, $parentCompany->getSubsidiaries());
        $this->assertSame([$childCompany1, $childCompany2], $parentCompany->getSubsidiaries());
    }

    /**
     * @test
     */
    public function can_record_employee_travel_data()
    {
        $company = new Company(['id' => "1", 'name' => 'ACME GLOBAL', 'parentId' => '0']);
        $this->assertCount(0, $company->getEmployeeTravelRecords());

        $travelRecords = [
            new Travel(['id' => '1', 'employeeName' => 'John', 'price' => '200', 'departure' => 'A', 'destination' => 'B', 'companyId' => '1']),
            new Travel(['id' => '2', 'employeeName' => 'Jane', 'price' => '100', 'departure' => 'A', 'destination' => 'B', 'companyId' => '1']),
            new Travel(['id' => '3', 'employeeName' => 'John', 'price' => '250', 'departure' => 'A', 'destination' => 'B', 'companyId' => '1']),
            new Travel(['id' => '4', 'employeeName' => 'Alex', 'price' => '50', 'departure' => 'A', 'destination' => 'B', 'companyId' => '1']),
        ];

        foreach ($travelRecords as $tr) {
            $company->addTravelRecord($tr);
        }

        $this->assertCount(4, $company->getEmployeeTravelRecords());
    }

    /**
     * @test
     */
    public function calculate_employee_travel_cost()
    {
        $company = new Company(['id' => "1", 'name' => 'ACME GLOBAL', 'parentId' => '0']);
        $travelRecords = [
            new Travel(['id' => '1', 'employeeName' => 'John', 'price' => '200', 'departure' => 'A', 'destination' => 'B', 'companyId' => '1']),
            new Travel(['id' => '2', 'employeeName' => 'Jane', 'price' => '100', 'departure' => 'A', 'destination' => 'B', 'companyId' => '1']),
            new Travel(['id' => '3', 'employeeName' => 'John', 'price' => '250', 'departure' => 'A', 'destination' => 'B', 'companyId' => '1']),
            new Travel(['id' => '4', 'employeeName' => 'Alex', 'price' => '50', 'departure' => 'A', 'destination' => 'B', 'companyId' => '1']),
        ];

        foreach ($travelRecords as $tr) {
            $company->addTravelRecord($tr);
        }

        $this->assertEquals(600, $company->calculateTravelCostForDirectEmployees());
    }

    /**
     * @test
     */
    public function calculate_cost_from_subsidiaries()
    {
        $company = new Company(['id' => "1", 'name' => 'ACME GLOBAL', 'parentId' => '0']);
        $this->assertEquals(0, $company->calculateCostFromSubsidiaries());

        $childCompany1 = new Company(["id" => "2", 'name' => 'ACME Asia', 'parentId' => '1']);
        $childCompany1->addTravelRecord(new Travel(['id' => '1', 'employeeName' => 'John', 'price' => '200', 'departure' => 'A', 'destination' => 'B', 'companyId' => '2']));

        $childCompany2 = new Company(["id" => "3", 'name' => 'ACME Europe', 'parentId' => '1']);
        $childCompany2->addTravelRecord(new Travel(['id' => '4', 'employeeName' => 'Alex', 'price' => '50', 'departure' => 'A', 'destination' => 'B', 'companyId' => '3']));

        $company->addSubsidiary($childCompany2);
        $company->addSubsidiary($childCompany1);

        $this->assertEquals(250, $company->calculateCostFromSubsidiaries());
    }

    /**
     * @test
     */
    public function company_total_cost_should_be_the_sum_of_employee_cost_and_subsidiaries_cost()
    {
        $company = new Company(['id' => "1", 'name' => 'ACME GLOBAL', 'parentId' => '0']);

        $company->addTravelRecord(new Travel(['id' => '1', 'employeeName' => 'Jane', 'price' => '100', 'departure' => 'A', 'destination' => 'B', 'companyId' => 1]));
        $company->addTravelRecord(new Travel(['id' => '2', 'employeeName' => 'Lynn', 'price' => '80', 'departure' => 'A', 'destination' => 'B', 'companyId' => 1]));

        $childCompany1 = new Company(["id" => "2", 'name' => 'ACME Asia', 'parentId' => 1]);
        $childCompany1->addTravelRecord(new Travel(['id' => '3', 'employeeName' => 'John', 'price' => '200', 'departure' => 'A', 'destination' => 'B', 'companyId' => '2']));

        $childCompany2 = new Company(["id" => "3", 'name' => 'ACME Europe', 'parentId' => 1]);
        $childCompany2->addTravelRecord(new Travel(['id' => '4', 'employeeName' => 'Alex', 'price' => '50', 'departure' => 'A', 'destination' => 'B', 'companyId' => '3']));

        $company->addSubsidiary($childCompany2);
        $company->addSubsidiary($childCompany1);

        $this->assertEquals(430, $company->getTotalCost());

        $childCompany3 = new Company(["id" => "30", 'name' => 'ACME Singapore', 'parentId' => 2]);
        $childCompany3->addTravelRecord(new Travel(['id' => '3', 'employeeName' => 'Ming', 'price' => '500', 'departure' => 'C', 'destination' => 'D', 'companyId' => '30']));
        $childCompany1->addSubsidiary($childCompany3);

        $this->assertEquals(930, $company->getTotalCost());

        $childCompany4 = new Company(["id" => "40", 'name' => 'ACME Spain', 'parentId' => 3]);
        $childCompany4->addTravelRecord(new Travel(['id' => '3', 'employeeName' => 'Ramos', 'price' => '70', 'departure' => 'E', 'destination' => 'F', 'companyId' => '40']));
        $childCompany2->addSubsidiary($childCompany4);

        $this->assertEquals(1000, $company->getTotalCost());
    }
}
