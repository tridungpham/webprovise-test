<?php

use PHPUnit\Framework\TestCase;

/**
 * @covers
 */
final class TravelTest extends TestCase
{
    /**
     * @test
     */
    public function can_init_travel_from_array_of_data()
    {
        $data = [
            "id" => "uuid-t1",
            "createdAt" => "2020-08-27T00:22:26.927Z",
            "employeeName" => "Garry Schuppe",
            "departure" => "Saint Kitts and Nevis",
            "destination" => "Pitcairn Islands",
            "price" => "362.00",
            "companyId" => "uuid-1"
        ];

        $travel = new Travel($data);
        $this->assertSame($data['id'], $travel->getId());
        $this->assertSame($data['employeeName'], $travel->getEmployeeName());
        $this->assertSame($data['departure'], $travel->getDeparture());
        $this->assertSame($data['destination'], $travel->getDestination());
        $this->assertSame($data['price'], $travel->getPriceAsString());
        $this->assertSame($data['companyId'], $travel->getCompanyId());
        $this->assertInstanceOf(DateTime::class, $travel->getCreatedTime());
    }
}
