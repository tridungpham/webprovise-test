<?php

require_once './vendor/autoload.php';

class TestScript
{
    public function execute() : void
    {
        [$companiesData, $travelData] = $this->fetchData();

        $start = microtime(true);
        /** @var array<string, Company> */
        
        $companies = $this->prepareCompaniesList($companiesData);
        $companies = $this->processTravelData($companies, $travelData);

        $result = array_map(
            fn(Company $c) => $c->toArray(),
            array_filter($companies, fn(Company $company) => $company->isFirstLevelCompany())
        );

        echo "Result: \n\n";
        print_r(json_encode($result, JSON_PRETTY_PRINT));
        echo "\n\n";
        echo 'Total time: '.  (microtime(true) - $start);
    }

    /**
     * @return array{array, array}
     */
    private function fetchData() : array
    {
        $companiesDataURL = 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/companies';
        $travelURL = 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/travels';
        $companiesData = json_decode(file_get_contents($companiesDataURL), true);
        $travelData = json_decode(file_get_contents($travelURL), true);

        return [$companiesData, $travelData];
    }

    /**
     * @param array<string, mixed> $companiesData
     * @return Company[]
     */
    private function prepareCompaniesList(array $companiesData) : array
    {
        $companies = [];
        foreach ($companiesData as $entry) {
            $company = new Company($entry);
            $companies[$company->getId()] = $company;

            if (array_key_exists($company->getParentId(), $companies)) {
                $companies[$company->getParentId()]->addSubsidiary($company);
            }
        }

        return $companies;
    }

    /**
     * @param Company[] $companies
     * @param array<string, mixed> $travelData
     * @return Company[]
     */
    private function processTravelData(array $companies, array $travelData) : array
    {
        foreach ($travelData as $travelRecordData) {
            $record = new Travel($travelRecordData);
            if (array_key_exists($record->getCompanyId(), $companies)) {
                $companies[$record->getCompanyId()]->addTravelRecord($record);
            }
        }

        return $companies;
    }
}

(new TestScript())->execute();