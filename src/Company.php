<?php

class Company
{
    private string $id;
    private string $name;
    private ?DateTime $createdAt = null;
    private string $parentId;

    /**
     * @var Company[]
     */
    private array $subsidiaries = [];

    /**
     * @var array<string, array<Travel>>
     */
    public array $employeeTravels = [];

    /**
     * @param array<string,string> $data
     */
    public function __construct(array $data)
    {
        $keys = [
            'id',
            'name',
            'parentId'
        ];

        foreach ($keys as $key) {
            if (!array_key_exists($key, $data)) {
                throw new InvalidArgumentException("Required field missing from initial data: {$key}");
            }

            $this->$key = $data[$key];
        }

        if (array_key_exists('createdAt', $data) && is_string($data['createdAt'])) {
            $this->createdAt = new DateTime($data['createdAt']);
        }

        if (array_key_exists('parentId', $data) && is_string($data['parentId'])) {
            $this->parentId = $data['parentId'];
        }
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function getName() : string
    {
        return $this->name;
    }

    public function getParentId() : string
    {
        return $this->parentId;
    }

    /**
     * Here we assume that first level company has parent id equal '0' 
     */
    public function isFirstLevelCompany() : bool
    {
        return $this->getParentId() == '0';
    }

    public function getCreatedTime() : ?DateTime
    {
        return $this->createdAt;
    }

    public function getTotalCost() : float
    {
        return $this->calculateTravelCostForDirectEmployees() + $this->calculateCostFromSubsidiaries();
    }

    public function calculateTravelCostForDirectEmployees() : float
    {
        $total = 0;
        foreach ($this->employeeTravels as $travelInfo) {
            foreach ($travelInfo as $travelRecord) {
                $total += $travelRecord->getPrice();
            }
        }

        return $total;
    }

    public function calculateCostFromSubsidiaries() : float
    {
        return array_sum(
            array_map(
                fn(Company $company) => $company->getTotalCost(), 
                $this->subsidiaries
            )
        );
    }

    public function addTravelRecord(Travel $travel) : void
    {
        if (!isset($this->employeeTravels[$travel->getEmployeeName()])) {
            $this->employeeTravels[$travel->getEmployeeName()] = [];
        }

        $this->employeeTravels[$travel->getEmployeeName()][] = $travel;
    }

    /**
     * @return Travel[]
     */
    public function getEmployeeTravelRecords() : array
    {
        return array_reduce($this->employeeTravels, fn(array $carry, array $item) => array_merge($carry, $item), []);
    }

    public function addSubsidiary(Company $company) : void
    {
        $this->subsidiaries[$company->getName()] = $company;
    }

    /**
     * @return Company[]
     */
    public function getSubsidiaries() : array
    {
        return array_values($this->subsidiaries);
    }

    /**
     * @return array<string, array<array<string, array|string>>|float|string>
     */
    public function toArray() : array
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'cost' => $this->getTotalCost(),
            'children' => array_map(
                fn(Company $subCompany) => $subCompany->toArray(),
                $this->getSubsidiaries()
            ),
            // 'employeesCost' => $this->calculateTravelCostForDirectEmployees(),
        ];
    }
}