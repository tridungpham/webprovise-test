<?php

class Travel
{
    private string $id;
    private string $employeeName;
    private string $departure;
    private string $destination;
    private float $price;
    private string $companyId;
    private ?DateTime $createdAt;

    /**
     * @param array<string,string|float> $data
     */
    public function __construct(array $data)
    {
        $keys = [
            'id',
            'employeeName',
            'departure',
            'destination',
            'price',
            'companyId',
        ];

        foreach ($keys as $key) {
            if (!array_key_exists($key, $data)) {
                throw new InvalidArgumentException("Required field missing from initial data: {$key}");
            }

            $this->$key = $data[$key];
        }

        if (array_key_exists('createdAt', $data) && is_string($data['createdAt'])) {
            $this->createdAt = new DateTime($data['createdAt']);
        }
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function getEmployeeName() : string
    {
        return $this->employeeName;
    }

    public function getDeparture() : string
    {
        return $this->departure;
    }

    public function getDestination() : string
    {
        return $this->destination;
    }

    public function getPrice() : float
    {
        return $this->price;
    }

    public function getPriceAsString() : string
    {
        return sprintf("%0.2f", $this->price);
    }

    public function getCompanyId() : string
    {
        return $this->companyId;
    }

    public function getCreatedTime() : ?DateTime
    {
        return $this->createdAt;
    }
}