### Prerequisite
- PHP >= 7.4
- [Composer](https://getcomposer.org)

### Instruction
- Clone the repository to local machine
- Run `composer dump-autoload` to initiate autoloader.
- Run `php index.php`  to get result ([Sample output](sample_output.json)).

### Testing
- Run `composer install` to fetch dependencies (PHPUnit).
- Run `composer test` to execute tests.
